#Splits array by ","
def sortTxtIntoArray():
    myList = []
    with open("C:/Users/Owner/Desktop/test.csv") as f:
        line = f.readline()
        #reads file until end of line
        while line:

            #Splits into array then strips
            listArray = line.split(',')
            itemName = (listArray[0].strip("\""))
            itemQty = (listArray[1].strip("\""))

            #Checkname function looks to see if itemname doesnt contain total and itemqty is a number, returns itemname
            x = checkName(itemName,itemQty)
            if type(x) == str:
                myList.append(x)

            #Reads next line then restarts loop unless no more lines
            line = f.readline()

    writeToFile(myList)

#Writes itemnames to a new file
def writeToFile(myList):
    f = open("C:/Users/Owner/Desktop/itemlist.csv", 'w')
    for i in myList:
         f.write(i + ",5" + "\n")
    f.close()

#Checkname function looks to see if itemname doesnt contain total and itemqty is a number, returns itemname
def checkName(itemName,itemQty):
    if (itemQty.isdigit()):
        itemQty = int(itemQty)

    if ("Total" or "total" or "TOTAL") not in itemName:
        if type(itemQty) == int:
            return itemName

sortTxtIntoArray()